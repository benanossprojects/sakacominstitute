up:
	docker compose up -d

down:
	docker compose down

build:
	docker compose build

run:
	docker compose up --build -d

# deploy:
# 	docker compose -f docker-compose-deploy.yml down --volumes

# build_deploy:
# 	docker compose -f docker-compose-deploy.yml build

build_run:
	docker compose build .
up_run:
	docker compose -f local.yml up
	
down_run:
	docker compose -f local.yml down

# migrate:
# 	docker-compose run web python manage.py migrate

# createsuperuser:
# 	docker compose run web python manage.py createsuperuser