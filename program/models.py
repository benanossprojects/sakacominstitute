from django.db import models
from django.utils.text import slugify
from django.urls import reverse

# Create your models here.


class Programs(models.Model):
    program_name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=200, unique=True, null=True, blank=True)
    program_image = models.ImageField(upload_to="images/programes", blank=True)

    class Meta:
        verbose_name = "Programs"
        verbose_name_plural = "Programs"

    def get_url(self):
        return reverse("frontend:courses_by_program", args=[self.slug])

    def save(self, *args, **kwargs):
        if not self.slug:
            # Generate slug from program name
            self.slug = slugify(self.program_name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.program_name
