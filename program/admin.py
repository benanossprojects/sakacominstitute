from django.contrib import admin
from .models import Programs

# @admin.register()
class ProgramAdmin(admin.ModelAdmin):
    list_display = ('program_name', 'slug', 'program_image')
# Register your models here.
admin.site.register(Programs, ProgramAdmin)
