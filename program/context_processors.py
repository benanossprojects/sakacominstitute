from .models import Programs


def menu_links(request):
    links = Programs.objects.all()
    return dict(links=links)
