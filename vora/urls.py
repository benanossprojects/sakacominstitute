from django.urls import path
from vora import vora_views
from users import users_views
from frontend import frontend_views

app_name = "vora"
urlpatterns = [
    path("users/", users_views.users, name="users"),
    path("user-details/<int:id>/", users_views.user_details, name="user-details"),
    path("add-user/", users_views.add_user, name="add-user"),
    path("edit-user/<int:id>/", users_views.edit_user, name="edit-user"),
    path("delete-user/<int:id>/", users_views.delete_user, name="delete-user"),
    path(
        "delete-multiple-user/",
        users_views.delete_multiple_user,
        name="delete-multiple-user",
    ),
    path("login/", users_views.login_user, name="login"),
    path("logout/", users_views.logout_user, name="logout"),
    path("groups/", users_views.groups_list, name="groups"),
    path("group-edit/<int:id>/", users_views.group_edit, name="group-edit"),
    path("group-delete/<int:id>/", users_views.group_delete, name="group-delete"),
    path("group-add/", users_views.group_add, name="group-add"),
    path("permissions/", users_views.permissions, name="permissions"),
    path(
        "edit-permissions/<int:id>/",
        users_views.edit_permissions,
        name="edit-permissions",
    ),
    path(
        "delete-permissions/<int:id>/",
        users_views.delete_permissions,
        name="delete-permissions",
    ),
    path(
        "assign-permissions-to-user/<int:id>/",
        users_views.assign_permissions_to_user,
        name="assign-permissions-to-user",
    ),
    path("signup/", users_views.signup, name="signup"),
    path("activate/<uidb64>/<token>/", users_views.activate, name="activate"),
    path("", vora_views.index, name="index"),
    path("index/", vora_views.index, name="index"),
    path("index_2/", vora_views.index_2, name="index_2"),
    path("projects/", vora_views.projects, name="projects"),
    path("contacts/", vora_views.contacts, name="contacts"),
    path("kanban/", vora_views.kanban, name="kanban"),
    path("calendar/", vora_views.calendar, name="calendar"),
    path("messages/", vora_views.messages, name="messages"),
    path("app-profile/", vora_views.app_profile, name="app-profile"),
    path("post-details/", vora_views.post_details, name="post-details"),
    path("email-compose/", vora_views.email_compose, name="email-compose"),
    path("email-inbox/", vora_views.email_inbox, name="email-inbox"),
    path("email-read/", vora_views.email_read, name="email-read"),
    path("app-calender/", vora_views.app_calender, name="app-calender"),
    path("ecom-product-grid/", vora_views.ecom_product_grid, name="ecom-product-grid"),
    path("ecom-product-list/", vora_views.ecom_product_list, name="ecom-product-list"),
    path(
        "ecom-product-detail/",
        vora_views.ecom_product_detail,
        name="ecom-product-detail",
    ),
    path(
        "ecom-product-order/", vora_views.ecom_product_order, name="ecom-product-order"
    ),
    path("ecom-checkout/", vora_views.ecom_checkout, name="ecom-checkout"),
    path("ecom-invoice/", vora_views.ecom_invoice, name="ecom-invoice"),
    path("ecom-customers/", vora_views.ecom_customers, name="ecom-customers"),
    path("chart-flot/", vora_views.chart_flot, name="chart-flot"),
    path("chart-morris/", vora_views.chart_morris, name="chart-morris"),
    path("chart-chartjs/", vora_views.chart_chartjs, name="chart-chartjs"),
    path("chart-chartist/", vora_views.chart_chartist, name="chart-chartist"),
    path("chart-sparkline/", vora_views.chart_sparkline, name="chart-sparkline"),
    path("chart-peity/", vora_views.chart_peity, name="chart-peity"),
    path("ui-accordion/", vora_views.ui_accordion, name="ui-accordion"),
    path("ui-alert/", vora_views.ui_alert, name="ui-alert"),
    path("ui-badge/", vora_views.ui_badge, name="ui-badge"),
    path("ui-button/", vora_views.ui_button, name="ui-button"),
    path("ui-modal/", vora_views.ui_modal, name="ui-modal"),
    path("ui-button-group/", vora_views.ui_button_group, name="ui-button-group"),
    path("ui-list-group/", vora_views.ui_list_group, name="ui-list-group"),
    path("ui-media-object/", vora_views.ui_media_object, name="ui-media-object"),
    path("ui-card/", vora_views.ui_card, name="ui-card"),
    path("ui-carousel/", vora_views.ui_carousel, name="ui-carousel"),
    path("ui-dropdown/", vora_views.ui_dropdown, name="ui-dropdown"),
    path("ui-popover/", vora_views.ui_popover, name="ui-popover"),
    path("ui-progressbar/", vora_views.ui_progressbar, name="ui-progressbar"),
    path("ui-tab/", vora_views.ui_tab, name="ui-tab"),
    path("ui-typography/", vora_views.ui_typography, name="ui-typography"),
    path("ui-pagination/", vora_views.ui_pagination, name="ui-pagination"),
    path("ui-grid/", vora_views.ui_grid, name="ui-grid"),
    path("uc-select2/", vora_views.uc_select2, name="uc-select2"),
    path("uc-nestable/", vora_views.uc_nestable, name="uc-nestable"),
    path("uc-noui-slider/", vora_views.uc_noui_slider, name="uc-noui-slider"),
    path("uc-sweetalert/", vora_views.uc_sweetalert, name="uc-sweetalert"),
    path("uc-toastr/", vora_views.uc_toastr, name="uc-toastr"),
    path("map-jqvmap/", vora_views.map_jqvmap, name="map-jqvmap"),
    path("uc-lightgallery/", vora_views.uc_lightgallery, name="uc-lightgallery"),
    path("uc-lightgallery/", vora_views.uc_lightgallery, name="uc-lightgallery"),
    path("widget-basic/", vora_views.widget_basic, name="widget-basic"),
    path("form-element/", vora_views.form_element, name="form-element"),
    path("form-wizard/", vora_views.form_wizard, name="form-wizard"),
    path(
        "form-editor-ckeditor/",
        vora_views.form_editor_ckeditor,
        name="form-editor-ckeditor",
    ),
    path("form-pickers/", vora_views.form_pickers, name="form-pickers"),
    path("form-validation/", vora_views.form_validation, name="form-validation"),
    path(
        "table-bootstrap-basic/",
        vora_views.table_bootstrap_basic,
        name="table-bootstrap-basic",
    ),
    path(
        "table-datatable-basic/",
        vora_views.table_datatable_basic,
        name="table-datatable-basic",
    ),
    path("page-lock-screen/", vora_views.page_lock_screen, name="page-lock-screen"),
    path("page-error-400/", vora_views.page_error_400, name="page-error-400"),
    path("page-error-403/", vora_views.page_error_403, name="page-error-403"),
    path("page-error-404/", vora_views.page_error_404, name="page-error-404"),
    path("page-error-500/", vora_views.page_error_500, name="page-error-500"),
    path("page-error-503/", vora_views.page_error_503, name="page-error-503"),
    # Slider
    path("slider-view/", frontend_views.slider_view, name="slider_view"),
    path("add-slider/", frontend_views.add_slider, name="add_slider"),
    path(
        "edit-slider/<int:slider_id>/", frontend_views.edit_slider, name="edit_slider"
    ),
    path(
        "delete_slider/<int:slider_id>/",
        frontend_views.delete_slider,
        name="delete_slider",
    ),
    # Events
    path("view-event/", frontend_views.event_view, name="event_view"),
    path("add-event/", frontend_views.add_event, name="add_event"),
    path("event/<int:event_id>/edit/", frontend_views.edit_event, name="edit_event"),
    path(
        "event/<int:event_id>/delete/", frontend_views.delete_event, name="delete_event"
    ),
    # About Page
    path("view-about/", frontend_views.about_view, name="about_view"),
    path("add-about/", frontend_views.add_about, name="add_about"),
    path("about/<int:about_id>/edit/", frontend_views.edit_about, name="edit_about"),
    path(
        "about/<int:about_id>/delete/", frontend_views.delete_about, name="delete_about"
    ),
    # Award
    path("view-award/", frontend_views.award_view, name="award_view"),
    path("add-award/", frontend_views.add_award, name="add_award"),
    path("award/<int:award_id>/edit/", frontend_views.edit_award, name="edit_award"),
    path(
        "award/<int:award_id>/delete/", frontend_views.delete_award, name="delete_award"
    ),
    # ============================Skill ==========================
    path("view-skill/", frontend_views.skill_view, name="skill_view"),
    path("add-skill/", frontend_views.add_skill, name="add_skill"),
    path("skill/<int:skill_id>/edit/", frontend_views.edit_skill, name="edit_skill"),
    path(
        "skill/<int:skill_id>/delete/", frontend_views.delete_skill, name="delete_skill"
    ),
    # ============================ Curriculum ==========================
    path("view-curriculum/", frontend_views.curriculum_view, name="curriculum_view"),
    path("add-curriculum/", frontend_views.add_curriculum, name="add_curriculum"),
    path(
        "curriculum/<int:curriculum_id>/edit/",
        frontend_views.edit_curriculum,
        name="edit_curriculum",
    ),
    path(
        "curriculum/<int:curriculum_id>/delete/",
        frontend_views.delete_curriculum,
        name="delete_curriculum",
    ),
    #   ====================== Course Category ===============
    path(
        "view-course-program/",
        frontend_views.course_program_view,
        name="course_program_view",
    ),
    path(
        "add-course-program/",
        frontend_views.add_course_program,
        name="add_course_program",
    ),
    path(
        "course-program/<int:course_program_id>/edit/",
        frontend_views.edit_course_program,
        name="edit_course_program",
    ),
    path(
        "course-program/<int:course_program_id>/delete/",
        frontend_views.delete_course_program,
        name="delete_course_program",
    ),
    #   ====================== Courses ===============
    path("view-course/", frontend_views.course_view, name="course_view"),
    path("add-course/", frontend_views.add_course, name="add_course"),
    path(
        "update-course/<slug:slug>/", frontend_views.update_course, name="update_course"
    ),
    path(
        "delete_course/<slug:slug>/", frontend_views.delete_course, name="delete_course"
    ),
    # path('add-course-rating/<slug:slug>/', frontend_views.add_course_rating, name='add_course_rating'),
    path("update-settings/", frontend_views.update_settings, name="update_settings"),
    #   ====================== Students Testimonials ===============
    path(
        "view-studemt-testimony/",
        frontend_views.student_testimonial_view,
        name="student_testimonial_view",
    ),
    path(
        "add-studemt-testimony/",
        frontend_views.add_student_testimonial,
        name="add_student_testimonial",
    ),
    path(
        "update-studemt-testimony/<int:student_testimonial_id>/edit/",
        frontend_views.edit_student_testimonial,
        name="edit_student_testimonial",
    ),
    path(
        "course-studemt-testimony/<int:student_testimonial_id>/delete/",
        frontend_views.delete_student_testimonial,
        name="delete_student_testimonial",
    ),
    path(
        "view-studemt-consultations/",
        frontend_views.student_consultation_view,
        name="student_consultation_view",
    ),
    path(
        "studemt-consultation/<int:student_consultation_id>/delete/",
        frontend_views.delete_student_consultation,
        name="delete_student_consultation",
    ),
]
