from django.contrib import admin
from frontend.models import (
    Slider,
    Event,
    About,
    Courses,
    Award,
    Setting,
    Skill,
    Curiculum,
    StudentTestimonial,
    Consultation
)

admin.site.register(Slider)
admin.site.register(About)
admin.site.register(Event)
admin.site.register(Award)
admin.site.register(Skill)
admin.site.register(Curiculum)
admin.site.register(Courses)
admin.site.register(StudentTestimonial)
admin.site.register(Consultation)
admin.site.register(Setting)


# Register your models here.
