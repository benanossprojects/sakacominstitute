import os
from django.conf import settings
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages

from frontend.forms import (
    AboutForm,
    AwardForm,
    CourseForm,
    CurriculumForm,
    # CourseCategoryForm,
    # CourseRatingForm,
    EventForm,
    ProgramForm,
    # ProgramForm,
    SettingsForm,
    SkillForm,
    SliderForm,
    StudentTestimonialForm,
)
from frontend.models import (
    About,
    Award,
    Consultation,
    Curiculum,
    # CourseCategory,
    Instructor,
    Courses,
    Setting,
    Skill,
    Slider,
    Event,
    StudentTestimonial,
)
from program.models import Programs

User = settings.AUTH_USER_MODEL


@login_required(login_url="vora:login")
def add_slider(request):
    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Slider item added successfully.")
            return redirect("vora:slider_view")
        else:
            messages.error(request, "Error adding slider item. Please check the form.")

    else:
        form = SliderForm()
    slider_items = Slider.objects.all()

    context = {"form": form, "slider_items": slider_items}

    return render(request, "vora/pages/home/slider/add-slider.html", context)


@login_required(login_url="vora:login")
def slider_view(request):
    slider_items = Slider.objects.all()
    return render(
        request,
        "vora/pages/home/slider/slider_view.html",
        {"slider_items": slider_items},
    )


@login_required(login_url="vora:login")
def edit_slider(request, slider_id):
    slider_item = get_object_or_404(Slider, id=slider_id)

    if request.method == "POST":
        form = SliderForm(request.POST, request.FILES, instance=slider_item)
        if form.is_valid():
            form.save()
            messages.success(request, "Slider updated successfully.")

            return redirect(
                "vora:slider_view"
            )  # Redirect to the slider view or any other appropriate page
    else:
        form = SliderForm(instance=slider_item)

    return render(
        request,
        "vora/pages/home/slider/edit_slider.html",
        {"form": form, "slider_item": slider_item},
    )


@login_required(login_url="vora:login")
def delete_slider(request, slider_id):
    slider_item = get_object_or_404(Slider, id=slider_id)

    if request.method == "POST":
        slider_item.delete()
        messages.success(request, "Slider deleted successfully.")

        return redirect(
            "vora:slider_view"
        )  # Redirect to the slider view or any other appropriate page

    return render(
        request,
        "vora/pages/home/slider/delete_slider.html",
        {"slider_item": slider_item},
    )


# About Page
@login_required(login_url="vora:login")
def about_view(request):
    about_data = About.objects.all()
    return render(
        request,
        "vora/pages/about/about_view.html",
        {"about_data": about_data},
    )


@login_required(login_url="vora:login")
def add_about(request):
    if request.method == "POST":
        form = AboutForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "About data added successfully.")
            return redirect(
                "vora:about_view"
            )  # Replace 'home' with the URL name for your home page
        else:
            messages.error(request, "Error adding About data. Please check the form.")
    else:
        form = AboutForm()
    about_data = About.objects.all()
    context = {"form": form, "about_data": about_data}
    return render(request, "vora/pages/about/add_about.html", context)


@login_required(login_url="vora:login")
def edit_about(request, about_id):
    about_data = get_object_or_404(About, pk=about_id)

    if request.method == "POST":
        form = AboutForm(request.POST, request.FILES, instance=about_data)
        if form.is_valid():
            form.save()
            messages.success(request, "About updated successfully!")
            return redirect(
                "vora:about_view"
            )  # Redirect to event_view without specifying event_id
        else:
            messages.error(request, "Form is invalid. Event not updated.")
            # Print form errors for debugging
            print(form.errors)

    else:
        form = AboutForm(instance=about_data)

    return render(request, "vora/pages/about/edit_about.html", {"form": form})


@login_required(login_url="vora:login")
def delete_about(request, about_id):
    about_data = get_object_or_404(About, id=about_id)
    if request.method == "POST":
        about_data.delete()
        messages.success(request, "Event deleted successfully!")
        return redirect("vora:about_view")  # Replace 'home' with your desired URL
    return render(
        request, "vora/pages/about/delete_view.html", {"about_data": about_data}
    )


# Events
@login_required(login_url="vora:login")
def event_view(request):
    event_data = Event.objects.all()
    return render(
        request,
        "vora/pages/event/event_view.html",
        {"event_data": event_data},
    )


@login_required(login_url="vora:login")
def add_event(request):
    if request.method == "POST":
        form = EventForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Event item added successfully.")
            return redirect(
                "vora:event_view"
            )  # Replace 'home' with the URL name for your home page
        else:
            messages.error(request, "Error adding Event. Please check the form.")
    else:
        form = EventForm()
    event_data = Event.objects.all()
    context = {"form": form, "event_data": event_data}
    return render(request, "vora/pages/event/add_event.html", context)


@login_required(login_url="vora:login")
def edit_event(request, event_id):
    event_data = get_object_or_404(Event, pk=event_id)

    if request.method == "POST":
        form = EventForm(request.POST, request.FILES, instance=event_data)
        if form.is_valid():
            form.save()
            messages.success(request, "Event updated successfully!")
            return redirect(
                "vora:event_view"
            )  # Redirect to event_view without specifying event_id
        else:
            messages.error(request, "Form is invalid. Event not updated.")
            # Print form errors for debugging
            print(form.errors)

    else:
        form = EventForm(instance=event_data)

    return render(request, "vora/pages/event/edit_event.html", {"form": form})


@login_required(login_url="vora:login")
def delete_event(request, event_id):
    event_data = get_object_or_404(Event, id=event_id)
    if request.method == "POST":
        event_data.delete()
        messages.success(request, "Event deleted successfully!")
        return redirect("vora:event_view")  # Replace 'home' with your desired URL
    return render(request, "vora/pages/delete_view.html", {"event_data": event_data})


# Awards
@login_required(login_url="vora:login")
def award_view(request):
    award_data = Award.objects.all()
    return render(
        request,
        "vora/pages/award/award_view.html",
        {"award_data": award_data},
    )


@login_required(login_url="vora:login")
def add_award(request):
    if request.method == "POST":
        form = AwardForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Award added successfully.")
            return redirect(
                "vora:award_view"
            )  # Replace 'home' with the URL name for your home page
        else:
            messages.error(request, "Error adding award. Please check the form.")
    else:
        form = AwardForm()
    award_data = Award.objects.all()
    context = {"form": form, "award_data": award_data}
    return render(request, "vora/pages/award/add_award.html", context)


@login_required(login_url="vora:login")
def edit_award(request, award_id):
    award_data = get_object_or_404(Award, pk=award_id)

    if request.method == "POST":
        form = AwardForm(request.POST, instance=award_data)
        if form.is_valid():
            form.save()
            messages.success(request, "Award updated successfully!")
            return redirect(
                "vora:award_view"
            )  # Redirect to event_view without specifying event_id
        else:
            messages.error(request, "Form is invalid. Award not updated.")
            # Print form errors for debugging
            print(form.errors)

    else:
        form = AwardForm(instance=award_data)

    return render(request, "vora/pages/award/edit_award.html", {"form": form})


@login_required(login_url="vora:login")
def delete_award(request, award_id):
    award_data = get_object_or_404(Award, id=award_id)
    if request.method == "POST":
        award_data.delete()
        messages.success(request, "Award deleted successfully!")
        return redirect("vora:award_view")  # Replace 'home' with your desired URL
    return render(
        request, "vora/pages/award/delete_view.html", {"award_data": award_data}
    )


# Skill
@login_required(login_url="vora:login")
def skill_view(request):
    skill_data = Skill.objects.all()
    return render(
        request,
        "vora/pages/skill/skill_view.html",
        {"skill_data": skill_data},
    )


@login_required(login_url="vora:login")
def add_skill(request):
    if request.method == "POST":
        form = SkillForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Skill added successfully.")
            return redirect(
                "vora:skill_view"
            )  # Replace 'home' with the URL name for your home page
        else:
            messages.error(request, "Error adding skill. Please check the form.")
    else:
        form = SkillForm()
    skill_data = Skill.objects.all()
    context = {"form": form, "skill_data": skill_data}
    return render(request, "vora/pages/skill/add_skill.html", context)


@login_required(login_url="vora:login")
def edit_skill(request, skill_id):
    skill_data = get_object_or_404(Skill, pk=skill_id)

    if request.method == "POST":
        form = SkillForm(request.POST, instance=skill_data)
        if form.is_valid():
            form.save()
            messages.success(request, "Skill updated successfully!")
            return redirect(
                "vora:skill_view"
            )  # Redirect to event_view without specifying event_id
        else:
            messages.error(request, "Form is invalid. Skill not updated.")
            # Print form errors for debugging
            print(form.errors)

    else:
        form = SkillForm(instance=skill_data)

    return render(request, "vora/pages/skill/edit_skill.html", {"form": form})


@login_required(login_url="vora:login")
def delete_skill(request, skill_id):
    skill_data = get_object_or_404(Skill, id=skill_id)
    if request.method == "POST":
        skill_data.delete()
        messages.success(request, "Skill deleted successfully!")
        return redirect("vora:skill_view")  # Replace 'home' with your desired URL
    return render(
        request, "vora/pages/skill/delete_view.html", {"skill_data": skill_data}
    )


# Curriculum
@login_required(login_url="vora:login")
def curriculum_view(request):
    curriculum_data = Curiculum.objects.all()
    return render(
        request,
        "vora/pages/curriculum/curriculum_view.html",
        {"curriculum_data": curriculum_data},
    )


@login_required(login_url="vora:login")
def add_curriculum(request):
    if request.method == "POST":
        form = CurriculumForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Curriculum added successfully.")
            return redirect(
                "vora:curriculum_view"
            )  # Replace 'home' with the URL name for your home page
        else:
            messages.error(request, "Error adding curriculum. Please check the form.")
    else:
        form = CurriculumForm()
    curriculum_data = Curiculum.objects.all()
    context = {"form": form, "curriculum_data": curriculum_data}
    return render(request, "vora/pages/curriculum/add_curriculum.html", context)


@login_required(login_url="vora:login")
def edit_curriculum(request, skill_id):
    curriculum_data = get_object_or_404(Curiculum, pk=skill_id)

    if request.method == "POST":
        form = CurriculumForm(request.POST, instance=curriculum_data)
        if form.is_valid():
            form.save()
            messages.success(request, "Curriculum updated successfully!")
            return redirect(
                "vora:curriculum_view"
            )  # Redirect to event_view without specifying event_id
        else:
            messages.error(request, "Form is invalid. Curriculum not updated.")
            # Print form errors for debugging
            print(form.errors)

    else:
        form = CurriculumForm(instance=curriculum_data)

    return render(request, "vora/pages/curriculum/edit_curriculum.html", {"form": form})


@login_required(login_url="vora:login")
def delete_curriculum(request, skill_id):
    curriculum_data = get_object_or_404(Curiculum, id=skill_id)
    if request.method == "POST":
        curriculum_data.delete()
        messages.success(request, "Curriculum deleted successfully!")
        return redirect("vora:curriculum_view")  # Replace 'home' with your desired URL
    return render(
        request,
        "vora/pages/curriculum/delete_view.html",
        {"curriculum_data": curriculum_data},
    )


# =========================== Settings ===================
@login_required(login_url="vora:login")
def update_settings(request):
    setting_instance, _ = Setting.objects.get_or_create(pk=1)

    if request.method == "POST":
        form = SettingsForm(request.POST, request.FILES, instance=setting_instance)
        if form.is_valid():
            old_image = setting_instance.logo
            new_image = form.cleaned_data["logo"]

            if old_image != new_image:
                old_image.delete()

            form.save()
            messages.success(request, "Settings updated successfully.")
            return redirect("vora:update_settings")
    else:
        form = SettingsForm(instance=setting_instance)

    return render(request, "vora/site_settings.html", {"form": form})


# # ========================= Course Category =================================
def course_program_view(request):
    course_program_data = Programs.objects.all()
    return render(
        request,
        "vora/pages/course-category/course_category_view.html",
        {"course_program_data": course_program_data},
    )


def add_course_program(request):
    if request.method == "POST":
        form = ProgramForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Program added successfully.")
            return redirect("vora:course_program_view")  # Redirect to a success URL
    else:
        form = ProgramForm()
    return render(
        request, "vora/pages/course-category/add_course_category.html", {"form": form}
    )


def edit_course_program(request, course_program_id):
    category = get_object_or_404(Programs, pk=course_program_id)
    if request.method == "POST":
        form = ProgramForm(request.POST, request.FILES, instance=category)
        if form.is_valid():
            form.save()
            messages.success(request, "Category updated successfully.")
            return redirect("vora:course_program_view")  # Redirect to a success URL
    else:
        form = ProgramForm(instance=category)
    return render(
        request, "vora/pages/course-category/edit_course_category.html", {"form": form}
    )


def delete_course_program(request, course_program_id):
    course_category = get_object_or_404(Programs, id=course_program_id)
    if request.method == "POST":
        course_category.delete()
        messages.success(request, "Category deleted successfully!")
        return redirect("vora:course_category_view")  # Redirect to a success URL
    return render(
        request,
        "vora/pages/course-category/delete_course_category.html",
        {"course_category": course_category},
    )


# # Courses
@login_required(login_url="vora:login")
def course_view(request):
    course_data = Courses.objects.all()
    return render(
        request,
        "vora/pages/courses/course_view.html",
        {"course_data": course_data},
    )


@login_required(login_url="vora:login")
def add_course(request):
    if request.method == "POST":
        form = CourseForm(request.POST, request.FILES)
        if form.is_valid():
            program = form.save(commit=False)
            # Associate the selected instructor with the program
            instructor = form.cleaned_data.get("instructor")
            if instructor:
                program.instructor = instructor
            program.save()
            messages.success(request, "Program item added successfully.")
            return redirect(
                "vora:course_view"
            )  # Replace 'program_view' with the URL name for your program view
        else:
            messages.error(request, "Error adding Program. Please check the form.")
    else:
        form = CourseForm()
    context = {"form": form}
    return render(request, "vora/pages/courses/add_course.html", context)


@login_required(login_url="vora:login")
def update_course(request, slug):
    program_data = get_object_or_404(Courses, slug=slug)

    if request.method == "POST":
        form = CourseForm(request.POST, request.FILES, instance=program_data)
        if form.is_valid():
            form.save()
            messages.success(request, "Program updated successfully!")
            return redirect(
                "vora:course_view"
            )  # Redirect to event_view without specifying event_id
        else:
            messages.error(request, "Form is invalid. Program not updated.")
            # Print form errors for debugging
            print(form.errors)

    else:
        form = CourseForm(instance=program_data)

    return render(request, "vora/pages/courses/edit_course.html", {"form": form})


@login_required(login_url="vora:login")
def delete_course(request, slug):
    program_data = get_object_or_404(Courses, slug=slug)
    if request.method == "POST":
        program_data.delete()
        messages.success(request, "Program deleted successfully!")
        return redirect("vora:course_view")  # Replace 'home' with your desired URL
    return render(
        request, "vora/pages/courses/delete_view.html", {"program_data": program_data}
    )


# Student Testimonials
def student_testimonial_view(request):
    testimonials = StudentTestimonial.objects.all()
    return render(
        request,
        "vora/pages/testimonials/student_testimonial_view.html",
        {"testimonials": testimonials},
    )


def add_student_testimonial(request):
    if request.method == "POST":
        form = StudentTestimonialForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Testimonial added successfully.")
            return redirect(
                "vora:student_testimonial_view"
            )  # Redirect to a success URL
    else:
        form = StudentTestimonialForm()
    return render(
        request, "vora/pages/testimonials/add_student_testimonial.html", {"form": form}
    )


def edit_student_testimonial(request, student_testimonial_id):
    testimonial = get_object_or_404(StudentTestimonial, pk=student_testimonial_id)
    if request.method == "POST":
        form = StudentTestimonialForm(request.POST, request.FILES, instance=testimonial)
        if form.is_valid():
            form.save()
            messages.success(request, "Testimonial updated successfully.")
            return redirect(
                "vora:student_testimonial_view"
            )  # Redirect to a success URL
    else:
        form = StudentTestimonialForm(instance=testimonial)
    return render(
        request, "vora/pages/testimonials/edit_student_testimonial.html", {"form": form}
    )


def delete_student_testimonial(request, student_testimonial_id):
    testimonial = get_object_or_404(StudentTestimonial, id=student_testimonial_id)
    if request.method == "POST":
        testimonial.delete()
        messages.success(request, "Testimonial deleted successfully!")
        return redirect("vora:student_testimonial_view")  # Redirect to a success URL
    return render(
        request,
        "vora/pages/testimonials/delete_student_testimonial.html",
        {"testimonial": testimonial},
    )


# Student Consultation
def student_consultation_view(request):
    student_consultations = Consultation.objects.all()
    return render(
        request,
        "vora/pages/students/student_consultation_view.html",
        {"student_consultations": student_consultations},
    )


def delete_student_consultation(request, student_consultation_id):
    consultation = get_object_or_404(Consultation, id=student_consultation_id)
    if request.method == "POST":
        consultation.delete()
        messages.success(request, "Consultation deleted successfully!")
        return redirect("vora:student_consultation_view")  # Redirect to a success URL
    return render(
        request,
        "vora/pages/students/delete_student_consultation.html",
        {"consultation": consultation},
    )
