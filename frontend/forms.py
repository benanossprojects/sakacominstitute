from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from frontend.models import (
    About,
    Award,
    Consultation,
    Curiculum,
    # CourseCategory,
    # CourseRating,
    Event,
    Courses,
    Setting,
    Skill,
    Slider,
    StudentTestimonial,
)
from django.utils.text import slugify

from program.models import Programs


class SliderForm(forms.ModelForm):
    class Meta:
        model = Slider
        fields = "__all__"
        widgets = {
            "image": forms.FileInput(),
        }

    welcome_title = forms.CharField(
        max_length=50,
        label="Title",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Title Here",
    )

    description = forms.CharField(
        label="Description",
        required=False,
        widget=CKEditorUploadingWidget(),
        initial="Description Here",
    )

    image = forms.ImageField(
        label="Slider image here",
        widget=forms.ClearableFileInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Description Here",
    )
    url_apply = forms.CharField(
        max_length=100,
        label="Add link",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Add Link Here",
    )
    url_read_more = forms.CharField(
        max_length=100,
        label="Add link",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Add Link Here",
    )

    def __init__(self, *args, **kwargs):
        super(SliderForm, self).__init__(*args, **kwargs)
        for key, field in self.fields.items():
            field.label = False


# About Form
class AboutForm(forms.ModelForm):
    class Meta:
        model = About
        fields = "__all__"
        widgets = {
            "image": forms.FileInput(),
            "image_2": forms.FileInput(),
            "image_3": forms.FileInput(),
            "email": forms.EmailInput(
                attrs={
                    "type": "email",
                    "class": "form-control",
                }
            ),
        }

    title = forms.CharField(
        max_length=50,
        label="Title",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="About Title Here",
    )

    description = forms.CharField(
        label="Description",
        required=False,
        widget=CKEditorUploadingWidget(),
        initial="About Description Here",
    )

    image = forms.ImageField(
        label="About image",
        widget=forms.ClearableFileInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="About image here",
    )

    more_details_link = forms.CharField(
        max_length=100,
        label="Add link",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Link here",
    )

    # def __init__(self, *args, **kwargs):
    #     super(AboutForm, self).__init__(*args, **kwargs)
    #     for key, field in self.fields.items():
    #         field.label = False


# Event Form
class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = "__all__"
        widgets = {
            "event_image": forms.FileInput(),
            "event_date": forms.DateTimeInput(
                attrs={
                    "type": "date",
                    "class": "form-control",
                }
            ),
            "start_time": forms.TimeInput(
                attrs={
                    "type": "time",
                    "class": "form-control",
                }
            ),
            "end_time": forms.TimeInput(
                attrs={
                    "type": "time",
                    "class": "form-control",
                }
            ),
        }

    title = forms.CharField(
        max_length=50,
        label="Title",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Event Title Here",
    )
    description = forms.CharField(
        label="Description",
        required=False,
        widget=CKEditorUploadingWidget(),
        # initial="Event Description Here",
    )

    event_image = forms.ImageField(
        label="About image",
        widget=forms.ClearableFileInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Event image here",
    )
    event_location = forms.CharField(
        max_length=150,
        label="Location",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Event location here",
    )
    phone_number = forms.CharField(
        max_length=150,
        label="Phone",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Phone number here",
    )
    registered_participants = forms.IntegerField(
        label="Registered participants",
        required=False,
        widget=forms.TextInput(
            attrs={
                "type": "number",
                "class": "form-control",
            }
        ),
        # initial="Phone number here",
    )
    slug = forms.CharField(
        max_length=150,
        label="Slug",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Slug",
    )

    def clean(self):
        cleaned_data = super().clean()
        title = cleaned_data.get("title")
        if title:
            cleaned_data["slug"] = slugify(title)
        return cleaned_data


# Award Form
class AwardForm(forms.ModelForm):
    class Meta:
        model = Award
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "award_number": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
        }


# Skill Form
class SkillForm(forms.ModelForm):
    class Meta:
        model = Skill
        fields = "__all__"
        widgets = {
            "skill_title": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "skill_description": forms.Textarea(
                attrs={
                    "class": "form-control",
                }
            ),
        }


# Curriculum Form
class CurriculumForm(forms.ModelForm):
    class Meta:
        model = Curiculum
        fields = "__all__"
        widgets = {
            "curiculum_title": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "curiculum_description": forms.Textarea(
                attrs={
                    "class": "form-control",
                }
            ),
        }

    # Settings Form


class SettingsForm(forms.ModelForm):

    class Meta:
        model = Setting
        fields = "__all__"
        widgets = {
            "logo": forms.FileInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "email": forms.EmailInput(
                attrs={
                    "type": "email",
                    "class": "form-control",
                }
            ),
            "name": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "phone_number": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "address": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
        }


class CourseForm(forms.ModelForm):
    class Meta:
        model = Courses
        fields = "__all__"
        widgets = {
            "course_name": forms.TextInput(attrs={"class": "form-control"}),
            "duration": forms.TextInput(attrs={"class": "form-control"}),
            "program": forms.Select(attrs={"class": "form-control"}),
            "description": forms.Textarea(attrs={"class": "form-control"}),
            "price": forms.TextInput(attrs={"class": "form-control"}),
            "course_content": forms.TextInput(attrs={"class": "form-control"}),
            "available_level": forms.TextInput(attrs={"class": "form-control"}),
            "start_time": forms.TimeInput(
                attrs={
                    "type": "time",
                    "class": "form-control",
                }
            ),
            "end_time": forms.TimeInput(
                attrs={
                    "type": "time",
                    "class": "form-control",
                }
            ),
            "skill_title": forms.TextInput(attrs={"class": "form-control"}),
            "overview_title": forms.TextInput(attrs={"class": "form-control"}),
            "overview_description": forms.Textarea(attrs={"class": "form-control"}),
            "curiculum_title": forms.TextInput(attrs={"class": "form-control"}),
            "curiculum_description": forms.Textarea(attrs={"class": "form-control"}),
            "level": forms.TextInput(attrs={"class": "form-control"}),
            "language": forms.TextInput(attrs={"class": "form-control"}),
            "cover_image": forms.FileInput(attrs={"class": "form-control"}),
            "read_more_link": forms.TextInput(attrs={"class": "form-control"}),
            "slug": forms.TextInput(attrs={"class": "form-control"}),
        }

    def clean(self):
        cleaned_data = super().clean()
        course_name = cleaned_data.get("course_name")
        if course_name:
            cleaned_data["slug"] = slugify(course_name)
        return cleaned_data


class ProgramForm(forms.ModelForm):

    class Meta:
        model = Programs
        fields = "__all__"
        widgets = {
            "program_name": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "program_image": forms.FileInput(
                attrs={
                    "class": "form-control",
                }
            ),
            "slug": forms.TextInput(
                attrs={
                    "class": "form-control",
                }
            ),
        }

        def clean(self):
            cleaned_data = super().clean()
            program_name = cleaned_data.get("program_name")
            if program_name:
                cleaned_data["slug"] = slugify(program_name)
            return cleaned_data


# class CourseRatingForm(forms.ModelForm):

#     class Meta:
#         model = CourseRating
#         fields = "__all__"
#         widgets = {
#             "course": forms.TextInput(
#                 attrs={
#                     "class": "form-control",
#                 }
#             ),
#             "user": forms.TextInput(
#                 attrs={
#                     "class": "form-control",
#                 }
#             ),
#             "stars": forms.TextInput(
#                 attrs={
#                     "class": "form-control",
#                 }
#             ),
#         }


# About Form
class StudentTestimonialForm(forms.ModelForm):
    class Meta:
        model = StudentTestimonial
        fields = "__all__"
        widgets = {
            "image": forms.FileInput(),
        }

    name = forms.CharField(
        label="Name",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Name",
    )
    course_studied = forms.CharField(
        label="Course & Year",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Course & Year",
    )
    content = forms.CharField(
        label="Content",
        required=False,
        widget=CKEditorUploadingWidget(),
        # initial="Event Description Here",
    )


# Consultaion
class ConsultationForm(forms.ModelForm):
    class Meta:
        model = Consultation
        fields = "__all__"

        widgets = {
            "program": forms.Select(attrs={"class": "form-control"}),
            "course": forms.Select(attrs={"class": "form-control"}),
        }

    client_name = forms.CharField(
        # label="Name",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        initial="Name",
    )
    client_address = forms.CharField(
        # label="Course & Year",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Course & Year",
    )
    client_number = forms.CharField(
        # label="Course & Year",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Course & Year",
    )
    client_email = (
        forms.EmailInput(
            attrs={
                "type": "email",
                "class": "form-control",
            }
        ),
    )
    client_subject = forms.CharField(
        # label="Course & Year",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Course & Year",
    )
    client_country = forms.CharField(
        # label="Course & Year",
        required=False,
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        ),
        # initial="Course & Year",
    )
    client_question = forms.CharField(
        # label="Content",
        required=False,
        widget=CKEditorUploadingWidget(),
        # initial="Event Description Here",
    )
