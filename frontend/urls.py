from django.urls import path
from . import views
from users.users_views import signup

app_name = "frontend"
urlpatterns = [
    path("", views.index, name="home"),
    path("about-us", views.about, name="about"),
    path("contact-us", views.contact, name="contact"),
    path("it-programs", views.it_programs, name="it_programs"),
    # path("all-programs", views.program_page, name="all_programs"),
    # Events
    path("events/", views.event_page, name="event_page"),
    path("detail/<slug:slug>/", views.event_detail, name="event_detail"),
    path("programs", views.all_programs, name="all_programs"),
    path("program/<slug:program_slug>/", views.all_programs, name="courses_by_program"),
    path(
        "program/<slug:program_slug>/<slug:course_slug>/",
        views.course_detail,
        name="course_detail",
    ),
    path("search/", views.search, name="search"),
    path("signup/", signup, name="signup"),
    path("consultation/", views.consultation_view, name="consultation"),
    path("get-courses/", views.get_courses, name="get_courses"),
]
