from django.http import JsonResponse
from django.shortcuts import redirect, render, get_object_or_404

from frontend.forms import ConsultationForm
from frontend.models import (
    About,
    Award,
    Consultation,
    Courses,
    Curiculum,
    Setting,
    Skill,
    Slider,
    Event,
    StudentTestimonial,
)
from program.models import Programs
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.contrib import messages


# Create your views here.


def index(request):
    # awards = Award.objects.all()
    awards = Award.objects.order_by("id")[:3]
    slider = Slider.objects.all()
    about_data = get_object_or_404(About, pk=1)
    events = Event.objects.all().order_by("-event_date")[:4]
    courses = Courses.objects.all().order_by("id")[:2]
    programs = Programs.objects.all()
    testimonials = StudentTestimonial.objects.all()
    # settings = get_object_or_404(Setting, pk=1)
    # settings = Setting.objects.first()

    context = {
        "slider": slider,
        "awards": awards,
        "about_data": about_data,
        "events": events,
        "programs": programs,
        "courses": courses,
        "page_title": "Home",
        "testimonials": testimonials,
    }
    return render(request, "frontend/index.html", context)


def about(request):
    awards = Award.objects.order_by("id")[:4]
    about_data = get_object_or_404(About, pk=1)
    settings = get_object_or_404(Setting, pk=1)
    context = {
        "awards": awards,
        "about_data": about_data,
        "settings": settings,
        "page_title": "About Us",
    }
    return render(request, "frontend/about.html", context)


def contact(request):

    context = {"page_title": "Contact Us"}
    return render(request, "frontend/contact.html", context)


def it_programs(request):
    return render(request, "frontend/programs/it_programs/all_it_programs.html")


# Events
def event_page(request):
    events = Event.objects.all()
    event = Event.objects.all().order_by("id")

    paginator = Paginator(event, 5)  # Set the number of items per page (e.g., 5)
    page_number = request.GET.get("page")
    paged_events = paginator.get_page(page_number)

    context = {"event": paged_events, "events": events, "page_title": "Events Page"}
    return render(request, "frontend/event/event_page.html", context)


def event_detail(request, slug):
    event = Event.objects.get(slug=slug)
    latest_events = Event.objects.order_by("-event_date")[:3]

    context = {
        "event": event,
        "latest_events": latest_events,
        "page_title": "Event Detail",
    }
    return render(request, "frontend/event/event_detail.html", context)


# def all_programs(request, program_slug=None):
#     if program_slug:
#         # Get the specific program based on the slug
#         program = get_object_or_404(Programs, slug=program_slug)
#         # Retrieve courses related to the program
#         courses = Courses.objects.filter(program=program).order_by("id")
#         paginator = Paginator(courses, 3)
#         page = request.GET.get("page")
#         paged_courses = paginator.get_page(page)
#         course_count = courses.count()
#         context = {
#             "program": program,
#             "courses": paged_courses,
#             "course_count": course_count,
#         }
#     else:
#         # Retrieve all programs
#         programs = Programs.objects.all()
#         context = {
#             "programs": programs,
#         }
#     return render(request, "frontend/programs/program_page.html", context)


def all_programs(request, program_slug=None):
    if program_slug:
        # Get the specific program based on the slug
        program = get_object_or_404(Programs, slug=program_slug)
        # Retrieve courses related to the program
        courses = Courses.objects.filter(program=program).order_by("id")
        paginator = Paginator(courses, 2)
        page_number = request.GET.get("page")
        paged_courses = paginator.get_page(page_number)
        course_count = courses.count()
        context = {
            "program": program,
            "courses": paged_courses,
            "course_count": course_count,
            "page_title": "All Courses",
        }
    else:
        # Retrieve all courses
        courses = Courses.objects.all().order_by("id")
        paginator = Paginator(courses, 2)
        page_number = request.GET.get("page")
        paged_courses = paginator.get_page(page_number)
        course_count = courses.count()
        context = {
            "courses": paged_courses,
            "course_count": course_count,
            "page_title": "All Courses",
        }
    return render(request, "frontend/programs/program_page.html", context)


def course_detail(request, program_slug, course_slug):
    courses = Courses.objects.all()
    skills = Skill.objects.all()
    curriculums = Curiculum.objects.all()

    try:
        single_course = Courses.objects.get(
            program__slug=program_slug, slug=course_slug
        )
    except Exception as e:
        raise e

    context = {
        "single_course": single_course,
        "courses": courses,
        "skills": skills,
        "curriculums": curriculums,
        "page_title": "Course Detail",
    }
    return render(request, "frontend/programs/program_detail.html", context)


def search(request):
    if "keyword" in request.GET:
        keyword = request.GET["keyword"]
        if keyword:
            courses = Courses.objects.order_by("-created_date").filter(
                Q(description__icontains=keyword) | Q(course_name__icontains=keyword)
            )
            course_count = courses.count()
    context = {"courses": courses, "course_count": course_count}
    return render(request, "frontend/partials/header.html", context)


def site_sttings(request):
    settings = get_object_or_404(Setting, pk=1)
    context = {"settings": settings}
    return render(request, "frontend/partials/header.html", context)


# views.py
from django.shortcuts import render, redirect


# def consultation_view(request):
#     if request.method == "POST":
#         form = ConsultationForm(request.POST)
#         if form.is_valid():
#             form.save()
#             messages.success(
#                 request, "Your consultation has been submitted successfully!"
#             )
#             return redirect("frontend:home")  # Redirect to success page
#         else:
#             messages.error(
#                 request,
#                 "There was an error processing your consultation. Please try again.",
#             )
#     else:
#         form = ConsultationForm()

#     return render(request, "frontend/partials/get_quote_modal.html", {"form": form})


def consultation_view(request):
    programs = Programs.objects.all()
    courses = Courses.objects.none()

    if request.method == "POST":
        client_name = request.POST.get("client-name")
        client_country = request.POST.get("client-country")
        client_email = request.POST.get("client-email")
        client_address = request.POST.get("client-address")
        client_number = request.POST.get("client-number")
        client_subject = request.POST.get("client-subject")
        client_question = request.POST.get("client-question")
        program_id = request.POST.get("program")
        course_id = request.POST.get("course")

        Consultation.objects.create(
            client_name=client_name,
            client_country=client_country,
            client_email=client_email,
            client_address=client_address,
            client_number=client_number,
            client_subject=client_subject,
            client_question=client_question,
            program_id=program_id,
            course_id=course_id,
        )
        messages.success(request, "Your form has been submitted successfully!")
        return redirect("frontend:home")
    elif "program" in request.GET:
        program_id = request.GET.get("program")
        if program_id:
            selected_program = Programs.objects.get(pk=program_id)
            courses = selected_program.course_set.all()  # Redirect to a success page
    else:
        context = {"programs": programs, "courses": courses}
        return render(request, "frontend/index.html", context)


def get_courses(request):
    program_id = request.GET.get("program_id")
    if program_id:
        courses = Courses.objects.filter(program_id=program_id)
        course_data = [{"id": course.pk, "name": course.name} for course in courses]
        return JsonResponse(course_data, safe=False)
    else:
        return JsonResponse([], safe=False)


# def contact_view(request):
#     if request.method == "POST":
#         stud_name = request.POST.get("client-name")
#         client_country = request.POST.get("client-country")
#         stud_mail = request.POST.get("client-email")
#         client_address = request.POST.get("client-address")
#         stud_number = request.POST.get("stud-number")
#         client_subject = request.POST.get("client-subject")
#         stud_message = request.POST.get("stud-message")

#         Consultation.objects.create(
#             client_name=client_name,
#             client_country=client_country,
#             client_email=client_email,
#             client_address=client_address,
#             client_number=client_number,
#             client_subject=client_subject,
#             client_question=client_question,
#         )
#         messages.success(request, "Your form has been submitted successfully!")
#         return redirect("frontend:home")  # Redirect to a success page
#     else:
#         return render(request, "frontend/index.html")
