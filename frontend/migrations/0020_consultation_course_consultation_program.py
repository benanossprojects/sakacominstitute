# Generated by Django 5.0.3 on 2024-04-15 10:09

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("frontend", "0019_rename_country_consultation_client_country"),
        ("program", "0003_alter_programs_slug"),
    ]

    operations = [
        migrations.AddField(
            model_name="consultation",
            name="course",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                to="frontend.courses",
            ),
        ),
        migrations.AddField(
            model_name="consultation",
            name="program",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.CASCADE,
                to="program.programs",
            ),
        ),
    ]
