from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse
from django.utils.text import slugify
from django.conf import settings

from program.models import Programs

User = settings.AUTH_USER_MODEL
# Create your models here.


class Slider(models.Model):
    welcome_title = models.CharField(max_length=100, blank=False, null=False)
    description = RichTextUploadingField()
    image = models.ImageField(upload_to="slider", null=True)
    url_apply = models.CharField(max_length=100, default="#")
    url_read_more = models.CharField(max_length=100, default="#")

    def __str__(self):
        return self.welcome_title


class About(models.Model):
    title = models.CharField(max_length=150, blank=False, null=False)
    description = RichTextUploadingField()
    image = models.ImageField(upload_to="about", null=True, blank=True)
    image_2 = models.ImageField(upload_to="about", null=True, blank=True)
    image_3 = models.ImageField(upload_to="about", null=True, blank=True)
    more_details_link = models.CharField(max_length=100, default="#")
    email = models.EmailField(default="info@sakacominstitute.com")

    def __str__(self):
        return self.title


class Event(models.Model):
    title = models.CharField(max_length=150, blank=False, null=False)
    description = RichTextUploadingField()
    event_date = models.DateField()
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    event_location = models.CharField(max_length=150, blank=False, null=False)
    registered_participants = models.IntegerField(null=True, blank=True)
    event_image = models.ImageField(upload_to="event_images", null=True, blank=True)
    phone_number = models.CharField(
        max_length=20, null=True, blank=True
    )  # Assuming a phone number can be stored as a string
    slug = models.SlugField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


# class CourseCategory(models.Model):
#     name = models.CharField(max_length=100)
#     image = models.ImageField(upload_to="images/course-category", null=True, blank=True)

#     class Meta:
#         verbose_name_plural = "Course Categories"

#     def __str__(self):
#         return self.name


class Instructor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username


class Courses(models.Model):
    course_name = models.CharField(max_length=150, blank=True, null=True)
    duration = models.CharField(max_length=150, blank=True, null=True)
    available_level = models.CharField(max_length=255, blank=True, null=True)
    program = models.ForeignKey(
        Programs, related_name="courses", on_delete=models.CASCADE, default=1
    )
    description = RichTextUploadingField()
    price = models.CharField(max_length=150, blank=False, null=False)
    course_content = models.CharField(max_length=150, blank=False, null=False)
    skill_title = models.CharField(max_length=150, blank=False, null=False, default="")
    skill_description = RichTextUploadingField(null=True, blank=True)
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    overview_title = models.CharField(max_length=255, blank=False, null=False)
    overview_description = RichTextUploadingField()
    curiculum_title = models.CharField(max_length=255, blank=False, null=False)
    curiculum_description = RichTextUploadingField()
    level = models.CharField(max_length=100, blank=False, null=False)
    language = models.CharField(max_length=100, blank=False, null=False)
    cover_image = models.ImageField(
        upload_to="images/program/cover", null=True, blank=True
    )
    read_more_link = models.CharField(max_length=100, default="#")
    slug = models.SlugField(null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    modified_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Courses"

    def get_url(self):
        return reverse("frontend:course_detail", args=[self.program.slug, self.slug])

    def save(self, *args, **kwargs):
        if not self.slug:
            # Generate slug from program name
            self.slug = slugify(self.course_name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.course_name


# class CourseRating(models.Model):
#     course = models.ForeignKey(Courses, on_delete=models.CASCADE, default='')
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     stars = models.IntegerField(choices=[(i, str(i)) for i in range(1, 6)])


# Skills
class Skill(models.Model):
    skill_title = models.CharField(max_length=150, blank=False, null=False)
    skill_description = RichTextUploadingField(max_length=150, blank=False, null=False)

    def __str__(self):
        return self.skill_title


# Curiculum
class Curiculum(models.Model):
    curiculum_title = models.CharField(max_length=150, blank=False, null=False)
    curiculum_description = RichTextUploadingField(
        max_length=150, blank=False, null=False
    )

    def __str__(self):
        return self.curiculum_title


# Awards
class Award(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    award_number = models.CharField(max_length=150, blank=False, null=False)

    def __str__(self):
        return self.name


# Students Testimonials
class StudentTestimonial(models.Model):
    name = models.CharField(max_length=150, blank=True, null=True)
    image = models.FileField(upload_to="images/students", null=True, blank=True)
    course_studied = models.CharField(max_length=150, blank=True, null=True)
    content = RichTextUploadingField(null=True, blank=True)

    def __str__(self):
        return self.name


# Student Consultation
class Consultation(models.Model):
    client_name = models.CharField(max_length=100, null=True, blank=True)
    client_email = models.EmailField(max_length=100, null=True, blank=True)
    client_address = models.CharField(max_length=100, null=True, blank=True)
    client_number = models.CharField(max_length=20, null=True, blank=True)
    client_country = models.CharField(max_length=20, null=True, blank=True)
    client_subject = models.CharField(max_length=100, null=True, blank=True)
    client_question = RichTextUploadingField(null=True, blank=True)
    program = models.ForeignKey(Programs, on_delete=models.CASCADE, default=1)
    course = models.ForeignKey(Courses, on_delete=models.CASCADE, default=1)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    def __str__(self):
        return self.client_name


class Setting(models.Model):
    name = models.CharField(max_length=150, blank=False, null=False)
    logo = models.FileField(upload_to="logo", null=True, blank=True)
    email = models.EmailField(default="info@sakacominstitute.com")
    phone_number = models.CharField(max_length=150, blank=False, null=False)
    address = models.CharField(
        max_length=255, blank=False, null=False, default="1234 Bigot Lapaz"
    )

    def delete(self, *args, **kwargs):
        self.logo.delete()
        super(Setting, self).delete(*args, **kwargs)

    def __str__(self):
        return self.name
